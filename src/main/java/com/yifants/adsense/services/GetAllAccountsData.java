

package com.yifants.adsense.services;

import com.google.api.services.adsense.AdSense;
import com.google.api.services.adsense.model.Account;
import com.google.api.services.adsense.model.Accounts;

/**
 * @author asus25@google.com (Your Name Here)
 *
 */
public class GetAllAccountsData {

  public static Accounts getAccounts(AdSense adsense, int maxPageSize) throws Exception {
    System.out.println("===============================获取所有账户列表==================================");

    // Retrieve account list in pages and display data as we receive it.
    String pageToken = null;
    Accounts accounts = null;
    do {
      accounts = adsense.accounts().list()
          .setMaxResults(maxPageSize)
          .setPageToken(pageToken)
          .execute();
      
      if (accounts.getItems() != null && !accounts.getItems().isEmpty()) {
        for (Account account : accounts.getItems()) {
          System.out.printf("账户ID: \"%s\" ，姓名: \"%s\" \n",
              account.getId(), account.getName());
        }
      } else {
        System.out.println("没有找到账户");
      }
      
      pageToken = accounts.getNextPageToken();
    } while (pageToken != null);

    return accounts;
  }
}
