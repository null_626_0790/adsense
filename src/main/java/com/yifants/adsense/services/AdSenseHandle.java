package com.yifants.adsense.services;

import com.google.api.services.adsense.AdSense;
import com.google.api.services.adsense.model.Accounts;
import com.google.api.services.adsense.model.AdClients;


import com.yifants.adsense.sys.AdSenseInit;

import java.io.IOException;

/**
 * @author huww
 *
 */
public class AdSenseHandle {
  
  
  public static void process(AdSense adsense){
    
    if(adsense!=null){
      try {               
        //开始处理
        Accounts accounts = GetAllAccountsData.getAccounts(adsense, AdSenseInit.MAX_LIST_PAGE_SIZE);
        if ((accounts.getItems() != null) && !accounts.getItems().isEmpty()) {
          // admob accountid
          String chosenAccount = AdSenseInit.chooseAccount(accounts);
          //System.out.println("chosenAccount="+chosenAccount);
          
          AdClients adClients = GetAllAdClientsData.getAdClients(adsense, chosenAccount, AdSenseInit.MAX_LIST_PAGE_SIZE);                    
          if ((adClients.getItems() != null) && !adClients.getItems().isEmpty()) {
        	System.out.println("adClients.items="+adClients.getItems().toString());
            // 得到一个广告客户ID,需要从AdClients得到数据
            String exampleAdClientId = adClients.getItems().get(0).getId();
            System.out.println("exampleAdClientId="+exampleAdClientId);                 
            GenerateReportData.getGenerateReportData(adsense, chosenAccount, exampleAdClientId,AdSenseInit.MAX_REPORT_PAGE_SIZE);
            
          } else {
            System.out.println("No ad clients found, unable to run remaining methods.");
          }
        }
      } catch (IOException e) {
        e.printStackTrace();
      } catch (Throwable t) {
        t.printStackTrace();
      }     
    }
  }
}
