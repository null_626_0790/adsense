
package com.yifants.adsense.services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.api.services.adsense.AdSense;
import com.google.api.services.adsense.AdSense.Accounts.Reports.Generate;
import com.google.api.services.adsense.model.AdsenseReportsGenerateResponse;
import com.yifants.adsense.base.Condition;
import com.yifants.adsense.excel.ExcelWriter;

/**
 * @author huww
 *
 */
public class GenerateReportData {

//最大数量的获得行分页的报告(API的限制)
 private static final int ROW_LIMIT = 5000;
//时间格式
 static final DateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

 /**
  * Runs this sample.
  *
  * @param adsense广告服务对象
  * @param accountId账户使用的ID
  * @param adClientId广告客户ID来运行报告
  * @param maxReportPageSize最大大小页面检索
  * @throws Exception
  */
 public static void getGenerateReportData(AdSense adsense, String accountId, String adClientId,
     int maxReportPageSize) throws Exception {
   System.out.println("=================================================================");
   System.out.printf("Running report for ad client %s\n", adClientId);
   System.out.println("=================================================================");

   // 准备生成报告条件.
   String startDate = "";
   String endDate = "";
   //使用condition中的json时间生成报表
   if(Condition.getIfuse()!=null &&Condition.getIfuse().equals("yes")){
     startDate = Condition.getStartDate();
     endDate = Condition.getEndDate();
   }else{
     Date today = new Date();
     Calendar calendar = Calendar.getInstance();
     calendar.setTime(today);
     calendar.add(Calendar.DATE, -7);
     Date oneWeekAgo = calendar.getTime();

     startDate = DATE_FORMATTER.format(oneWeekAgo);
     endDate = DATE_FORMATTER.format(today);
   }
   
	long begin = System.currentTimeMillis();
   System.out.println("=============================开始请求报表============================");
   if(Condition.getIfuse()!=null &&Condition.getIfuse().equals("yes")){
	   System.out.println("使用condition查询条件:"+"ifuse:"+Condition.getIfuse()+",accountId:"+accountId+",startDate:"+startDate+",endDate:"+endDate+",currency:"+Condition.getCurrency());
   }else{
	   System.out.println("系统默认查询条件:"+"accountId:"+accountId+",startDate:"+startDate+",endDate:"+endDate);
   }
   Generate request = adsense.accounts().reports().generate(accountId, startDate, endDate);
   request.buildHttpRequest().setConnectTimeout(300000).setReadTimeout(300000);
   //request.buildHttpRequestUsingHead().setConnectTimeout(300000).setReadTimeout(300000);
   //System.out.println("getConnectTimeout="+request.buildHttpRequestUsingHead().getConnectTimeout());
   // 指定所需的广告客户使用一个过滤器
   request.setFilter(Arrays.asList("AD_CLIENT_ID==" + escapeFilterParameter(adClientId)));
   //报告依据的指标
   //"AD_UNIT_ID","AD_UNIT_NAME","PAGE_VIEWS", "AD_REQUESTS", "AD_REQUESTS_COVERAGE", "CLICKS","AD_REQUESTS_CTR", "COST_PER_CLICK", "AD_REQUESTS_RPM", "EARNINGS"
   request.setMetric(Arrays.asList("AD_UNIT_ID","AD_UNIT_NAME","AD_REQUESTS", "MATCHED_AD_REQUESTS","AD_REQUESTS_COVERAGE","INDIVIDUAL_AD_IMPRESSIONS","CLICKS",
       "INDIVIDUAL_AD_IMPRESSIONS_CTR", "INDIVIDUAL_AD_IMPRESSIONS_RPM", "EARNINGS"));
   //维度条件
   request.setDimension(Arrays.asList("DATE","AD_UNIT_ID","AD_UNIT_NAME"));
   //美元货币
   request.setCurrency((Condition.getCurrency()!=null && !Condition.getCurrency().equals(""))?Condition.getCurrency():"USD"); 
   //要使用帐户的本地时区请求报告，您需要将useTimezoneReporting参数设置为true（该参数的默认值是false,默认值是太平洋时区）
   request.setUseTimezoneReporting(true); 
   // 按升序排序
   request.setSort(Arrays.asList("+DATE"));

   // 运行结果的第一页请求数设置。
   request.setMaxResults(maxReportPageSize);
   AdsenseReportsGenerateResponse response = request.execute();

   if (response.getRows() == null || response.getRows().isEmpty()) {
     System.out.println("No rows returned.");
     return;
   }

   //英文转中文标题
   List<String> headers = new ArrayList<>();
   //excel数据存储
   List<List<String>> results = new ArrayList<List<String>>();
   // 第一页,设置显示的标题。
   displayHeaders(response.getHeaders(),headers,request.getCurrency());
   //System.out.println("response="+response.toString());
   // 显示第一页的返回结果数据
   results.addAll(response.getRows());
   //比较当前设置显示函数和最大限制显示的行数的大小,返回值为小的那个数
   int totalRows = Math.min(response.getTotalMatchedRows().intValue(), ROW_LIMIT);
   System.out.println("totalMatchedRows(总计行数)="+response.getTotalMatchedRows().intValue());
   System.out.println("totalRows(能取得的总行数)="+totalRows);
   System.out.println("first response.rows (第一页返回数据数)="+response.getRows().size());
   //System.out.println("first totals="+response.getTotals());
   for (int startIndex = response.getRows().size(); startIndex < totalRows;
       startIndex += response.getRows().size()) {

     //看看我们要超过极限,让尽可能返回多的结果集。
     int pageSize = Math.min(maxReportPageSize, totalRows - startIndex);
     request.setStartIndex(startIndex);
     request.setMaxResults(pageSize);
     // 获取下一条结果集
     response = request.execute();

     // 如果报告大小变化之间的分页请求,结果可能是空的
     if (response.getRows() == null || response.getRows().isEmpty()) {
       break;
     }
     //System.out.println("next totals="+response.getTotals());
     // 显示结果集
     results.addAll(response.getRows());
   }
   // 报告的总计行 该行与报告中的其他行长度相同；与维度列对应的单元格为空。
   //results.add(response.getTotals());
   long end = System.currentTimeMillis();
   System.out.printf("=============================报表请求完毕,耗时:%s 分钟=============================\n",(end-begin)/(1000*60));
   //生成excel文件
   if(null!=headers && null!=results){
     String sheetName = "admob-report";
     int size = headers.size();
     String[] arrHeaders = ((String[])headers.toArray(new String[size]));
     ExcelWriter.createExcelFile(sheetName, arrHeaders, results, startDate, endDate);
   }
 }

 /**
  * 显示报告的标题
  * @param 标题标题显示的列表
  */
 private static void displayHeaders(List<AdsenseReportsGenerateResponse.Headers> headers,List<String> arrHeaders,String currency) {
   if(null!=headers){
     for (AdsenseReportsGenerateResponse.Headers header : headers) {
       switch(header.getName()){
         case "DATE" : arrHeaders.add("日期");
          break;
        case "AD_UNIT_ID" : arrHeaders.add("应用");    
          break;
        case "AD_UNIT_NAME" : arrHeaders.add("广告单元");
          break;
        /*case "PAGE_VIEWS" : arrHeaders.add("网页浏览量");
          break;*/
        case "AD_REQUESTS" : arrHeaders.add("AdMob 广告网络请求次数");
          break;
        case "MATCHED_AD_REQUESTS" : arrHeaders.add("匹配请求数");
        break;
        case "AD_REQUESTS_COVERAGE" : arrHeaders.add("匹配率");
          break;
        case "INDIVIDUAL_AD_IMPRESSIONS" : arrHeaders.add("展示次数");
          break;
        case "CLICKS" : arrHeaders.add("点击次数");
        break;
        case "INDIVIDUAL_AD_IMPRESSIONS_CTR" : arrHeaders.add("展示点击率");
          break;
        /*case "COST_PER_CLICK" : arrHeaders.add("点击收入");
          break;*/
        case "INDIVIDUAL_AD_IMPRESSIONS_RPM" : arrHeaders.add("每千次展示收入"+" ( "+currency+")");
          break;
        case "EARNINGS" : arrHeaders.add("估算收入 ("+currency+")");
       }       
     }
   }
 }

 /**
  * 标题标题显示的列表
  * @param 行显示的列表。
  */
 public static void displayRows(List<List<String>> rows) {
   // Display results.
   for (List<String> row : rows) {
     for (String column : row) {
       System.out.printf("%25s", column);
     }
     System.out.println();
   }
 }

 /**
  * 特殊字符转义为参数被用于一个过滤器
  * @param parameter the parameter to be escaped.
  * @return the escaped parameter.
  */
 public static String escapeFilterParameter(String parameter) {
   return parameter.replace("\\", "\\\\").replace(",", "\\,");
 }
}
