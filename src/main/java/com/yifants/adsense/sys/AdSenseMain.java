
package com.yifants.adsense.sys;

import java.io.File;

import com.google.api.services.adsense.AdSense;
import com.yifants.adsense.base.Condition;
import com.yifants.adsense.services.AdSenseHandle;

/**
 * @author huww
 *
 */
public class AdSenseMain {

  /**
   * 主入口运用程序
   * conf 配置路径:C:/adsense/conf 
   * @param args
   */
  public static void main(String[] args) {
    
    String osName = System.getProperties().getProperty("os.name").toLowerCase();                                
    if(osName.contains("window")){
        if(args == null || args.length==0){
            args = new String [1];
            args[0] = "E:/javapro/adsense/conf";
            System.out.println("配置文件路径:"+args[0]);
        }
    }
    
    if((args != null) && (args.length > 0) ){
      String conf = args[0];
      File file = new File(conf);
      if (!file.exists() || file.isFile()) {
        System.out.println("配置文件目录不存在，请检查!");
        System.exit(1);
      }else{
    	System.out.println("配置文件路径:"+args[0]);         
        try {
            //加载配置文件
        	System.out.println("load sys.properties config ... ");
            String configFile = conf + File.separator + "sys.properties";
            SystemConfig.load(configFile);
        } catch (Exception e) {
            System.out.println("load sys.properties config exception" + e.getMessage());
        }
        
        try {
          //加载condition文件
        	System.out.println("load condition.json config ... ");
          String conditionFile = conf + File.separator + "condition.json";
          Condition.load(conditionFile);
        } catch (Exception e) {
          System.out.println("load condition.json config exception" + e.getMessage());
        }
        //调用程序
        AdSense adsense = AdSenseInit.initAdSense(conf);
        if(adsense!=null){
          AdSenseHandle.process(adsense);
        }
      }    
    }else{
      System.out.println("配置文件为空，请检查配置文件!");
      System.exit(1);   
    }  
  }
}
