package com.yifants.adsense.sys;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

public class SystemConfig {

    private static Properties p = null;

    public static void load(String configPath) throws Exception {
        File file = new File(configPath);
        p = new Properties();
        p.load(new FileReader(file));
    }

    //获取生成excel文件存放目录
    public static String getExcelFilePath(){     
        if(null==p.getProperty("excel.path") || p.getProperty("excel.path").equals("")){
            return "C:/adsense/excel/";
        }
        return p.getProperty("excel.path");
    }
    
    /**
     * 取得密钥证书路径
     * @return
     */
    public static String getStorePath(){
    	if(p.getProperty("store.path")!=null && !"".equals(p.getProperty("store.path"))){
            return p.getProperty("store.path");
        }
        return "";
    }
}
