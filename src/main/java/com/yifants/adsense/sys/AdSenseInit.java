
package com.yifants.adsense.sys;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.adsense.AdSense;
import com.google.api.services.adsense.AdSenseScopes;
import com.google.api.services.adsense.model.Accounts;
import com.google.api.services.adsense.model.AdsenseReportsGenerateResponse;

/**
 * huww
 *
 */
public class AdSenseInit {
  
  public static final String APPLICATION_NAME = "";

  /** 目录存储用户凭证 */
  private static final java.io.File DATA_STORE_DIR = SystemConfig.getStorePath().equals("")?
      new java.io.File(System.getProperty("user.home"), ".store/adsense_management_sample"):new java.io.File(SystemConfig.getStorePath());

  private static FileDataStoreFactory dataStoreFactory;

  /** 全球JSON工厂的实例 */
  private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

  // 请求参数设定.
  public static final int MAX_LIST_PAGE_SIZE = 50;
  public static final int MAX_REPORT_PAGE_SIZE = 50;

  /** 公共的HTTP传输的实例 */
  private static HttpTransport httpTransport;

  //private static Scanner scan;

  /** 授权已安装的应用程序访问用户的受保护的数据. */
  private static Credential authorize(String conf) throws Exception {
    // load client secrets
    GoogleClientSecrets clientSecrets = null;
    if(conf==null || conf.equals("")){
      System.out.println("client_secrets.json path:"+AdSenseInit.class.getResourceAsStream("/client_secrets.json"));
      clientSecrets = GoogleClientSecrets.load(JSON_FACTORY,
          new InputStreamReader(AdSenseInit.class.getResourceAsStream("/client_secrets.json")));
    }else{
      System.out.println("conf client_secrets.json path:"+conf+"/client_secrets.json");
      clientSecrets = GoogleClientSecrets.load(JSON_FACTORY,
          new InputStreamReader(new FileInputStream(conf+"/client_secrets.json")));
    }
    
    if (clientSecrets.getDetails().getClientId().startsWith("Enter")
        || clientSecrets.getDetails().getClientSecret().startsWith("Enter ")) {
      System.out.println("Enter Client ID and Secret from "
          + "https://code.google.com/apis/console/?api=adsense into "
          + "adsense-cmdline-sample/src/main/resources/client_secrets.json");
      System.exit(1);
    }
    // set up authorization code flow
    GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
        httpTransport, JSON_FACTORY, clientSecrets,
        Collections.singleton(AdSenseScopes.ADSENSE_READONLY)).setDataStoreFactory(
        dataStoreFactory).build();
    // authorize
    return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
  }

  /**
   * 初始化请求api
   * @return 一个AdSense服务对象初始化。
   * @throws Exception
   */
  private static AdSense initializeAdsense(String conf) throws Exception {
    // Authorization.
    Credential credential = authorize(conf);

    // Set up AdSense Management API client.
    AdSense adsense = new AdSense.Builder(
        new NetHttpTransport(), JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME)
        .build();

    return adsense;
  }

  public static AdSense initAdSense(String conf){
    try {
      //初始化条件
      httpTransport = GoogleNetHttpTransport.newTrustedTransport();
      System.out.println("DATA_STORE_DIR(登陆凭证存放地址):"+DATA_STORE_DIR.getPath());
      dataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);
      AdSense adsense = initializeAdsense(conf);
      return adsense;
    } catch (IOException e) {
    	e.printStackTrace();
    } catch (Throwable t) {
      t.printStackTrace();
    }
    return null;
  }

  /**
   * Lists all AdSense accounts the user has access to, and prompts them to choose one.
   *
   * @param accounts the list of accounts to choose from.
   * @return the ID of the chosen account.
   */
  public static String chooseAccount(Accounts accounts) {
    String accountId = null;

    if (accounts == null || accounts.getItems() == null || accounts.getItems().size() == 0) {
      System.out.println("没有 AdSense account");
      System.exit(-1);
    } else if (accounts.getItems().size() == 1) {
      accountId = accounts.getItems().get(0).getId();
      System.out.printf("使用 account  (%s) \n", accountId);
    } else {
      /*System.out.println("Please choose one of the following accounts:");
      for (int i = 0; i < accounts.getItems().size(); i++) {
        Account account = accounts.getItems().get(i);
        System.out.printf("%d. %s (%s)\n", i + 1, account.getName(), account.getId());
      }
      System.out.printf("> ");
      scan = new Scanner(System.in);
      accountId = accounts.getItems().get(scan.nextInt() - 1).getId();
      System.out.printf("Account %s chosen, resuming.\n", accountId);*/
    }

    //System.out.println();
    return accountId;
  }

  /**
   * Fills in missing date ranges from a report. This is needed because null
   * data for a given period causes that reporting row to be ommitted, rather than
   * set to zero (or the appropriate missing value for the metric in question).
   *
   * NOTE: This code assumes you have a single dimension in your report, and that
   * the dimension is either DATE or MONTH. The number of metrics is not relevant.
   *
   * @param response the report response received from the API.
   * @return the full set of report rows, complete with missing dates.
   */
  public static List<List<String>> fillMissingDates(AdsenseReportsGenerateResponse response)
      throws ParseException {
    DateFormat fullDate = new SimpleDateFormat("yyyy-MM-dd");
    Date startDate = fullDate.parse(response.getStartDate());
    Date endDate = fullDate.parse(response.getEndDate());

    int currentPos = 0;

    // Check if the results fit the requirements for this method.
    if (response.getHeaders() == null && !response.getHeaders().isEmpty()) {
      throw new RuntimeException("No headers defined in report results.");
    }

    if (response.getHeaders().size() < 2 ||
        !response.getHeaders().get(0).getType().equals("DIMENSION")) {
      throw new RuntimeException("Insufficient dimensions and metrics defined.");
    }

    if (response.getHeaders().get(1).getType().equals("DIMENSION")) {
      throw new RuntimeException("Only one dimension allowed.");
    }

    DateFormat dateFormat = null;
    Date date = null;
    // Adjust output format and start date according to time period.
    if (response.getHeaders().get(0).getName().equals("DATE")) {
      dateFormat = fullDate;
      date = startDate;
    } else if (response.getHeaders().get(0).getName().equals("MONTH")) {
      dateFormat = new SimpleDateFormat("yyyy-MM");
      date = fullDate.parse(dateFormat.format(startDate) + "-01");
    } else {
      // Return existing report rows without date filling.
      return response.getRows();
    }

    List<List<String>> processedData = new ArrayList<List<String>>();

    while (date.compareTo(endDate) <= 0) {
      Date rowDate = null;
      List<String> currentRow = null;
      if (response.getRows() != null && response.getRows().size() > currentPos) {
        currentRow = response.getRows().get(currentPos);
        // Parse date on current row.
        if (response.getHeaders().get(0).getName().equals("DATE")) {
          rowDate = fullDate.parse(currentRow.get(0));
        } else if (response.getHeaders().get(0).getName().equals("MONTH")) {
          rowDate = fullDate.parse(currentRow.get(0) + "-01");
        }
      }

      // Is there an entry for this date?
      if (rowDate != null && date.equals(rowDate)) {
        processedData.add(currentRow);
        currentPos += 1;
      } else {
        List<String> newRow = new ArrayList<String>();
        newRow.add(dateFormat.format(date));
        for (int i = 1; i < response.getHeaders().size(); i++) {
          newRow.add("no data");
        }
        processedData.add(newRow);
      }

      // Increment date accordingly.
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(date);
      if (response.getHeaders().get(0).getName().equals("DATE")) {
        calendar.add(Calendar.DATE, 1);
      } else if (response.getHeaders().get(0).getName().equals("MONTH")) {
        calendar.add(Calendar.MONTH, 1);
      }
      date = calendar.getTime();
    }

    return processedData;
  }
}
