package com.yifants.adsense.excel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormat;

import com.yifants.adsense.base.Condition;
import com.yifants.adsense.base.FileUtil;
import com.yifants.adsense.sys.SystemConfig;

/**
 * 
 * @author huww
 *
 */
public class ExcelWriter {

    /**
     * 生成excel文件
     * 
     * @param sheetName admob-report
     * @param headers
     * @param list
     * @throws Exception
     */
    public static void createExcelFile(String sheetName, String[] headers, List<List<String>> list,String startDate,String endDate) throws Exception{
    	
    	long begin = System.currentTimeMillis();
        System.out.println("=============================开始生成excel====================================");
        //生成报表的路径
        String dir = SystemConfig.getExcelFilePath(); 
        //目录不存在即创建
        FileUtil.mkdirs(dir);
        // 生成一个新的excel文件
        HSSFWorkbook workbook = new HSSFWorkbook();
        // 生成一个新的sheet
        HSSFSheet sheet = workbook.createSheet(sheetName);
        // 生成表头并插入表头
        HSSFRow row = sheet.createRow(0);
        addHeader(headers, row);
        
        //将单元id替换为应用名称并过滤掉不需要显示的数据
        for(int i=0;i<list.size();i++){
        	List<String> vlist = list.get(i);
        	if(vlist.size()>2){
        		String capp = vlist.get(1);
        		if(capp.contains("ca-app-pub") && capp.contains(":")){
        			String appname = replaceAppName(capp);
        			if(!appname.equals("")){
        				vlist.set(1, appname);
        			}else{
        				list.remove(vlist);
        				i--;
        			}      			
            	}
        	}
        }
    	//对数据依应用名称分组
        if(Condition.getIsgroup()!=null && Condition.getIsgroup().equals("yes")){
        	Map<String, List<List<String>>> mapList = groupByappname(list);
            List<List<String>> groupList = getAllGroup(mapList);
            list=groupList;
        }     
        
        // 插入数据项
        try {
          insertData(workbook,list, sheet);          
        } catch (Exception e1) {
          e1.printStackTrace();
          //System.err.println("插入数据出错");        
        }

        // 设置列宽
        for (int n = 0; n < headers.length; n++) {
            sheet.setColumnWidth(n, sheet.getColumnWidth(n) * 2);
        }

        // 写文件
        FileOutputStream fis = null;
        try {
            String fileName = dir + "/admob-report-"+System.currentTimeMillis()+"(" + startDate + "-"+ endDate + ").xls";
            if(new File(fileName).exists()){
              System.out.println("文件名存在");
            }
            fis = new FileOutputStream(fileName);                
            workbook.write(fis);
        } catch (Exception e) {
        e.printStackTrace();
          //System.err.println( "写excel文件出错");
        } finally {
            if (null != fis) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                fis = null;
            }
        }
        long end = System.currentTimeMillis();
		//System.out.println("天统计结束，耗时："+(end-begin)/1000*60 +"分钟");
        System.out.printf("=============================生成excel完毕,耗时:%s 分钟=============================\n",(end-begin)/(1000*60));
        //System.out.println("报表处理结束");
    }

    /**
     * 添加excel表头
     * 
     * @param headers
     * @param row
     */
    @SuppressWarnings("deprecation")
	private static void addHeader(String[] headers, HSSFRow row) {
        for (short i = 0; i < headers.length; i++) {
            HSSFCell cell = row.createCell(i);
            row.setHeight((short) 400);
            cell.setCellValue(headers[i]);
        }
    }

    /**
     * 插入数据
     * 
     * @param list
     * @param sheet
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * 
     */
    private static void insertData(HSSFWorkbook workbook,List<List<String>> list, HSSFSheet sheet) 
        throws NoSuchMethodException, IllegalAccessException,InvocationTargetException {
        if (null != list) {
            HSSFRow row;
            for (int j = 0; j < list.size(); j++) {
                List<String> clist = list.get(j);
                row = sheet.createRow(j + 1);
                for (int i = 0; i < clist.size(); i++) {
                    HSSFCell cell = row.createCell(i);
                    String value = clist.get(i);
                    if(null!=value&&!value.equals("")){
                      if(isNumeric(value)){
                        if(value.contains(".")){
                          //setTwoPoint(workbook, cell, value);
                          cell.setCellValue(Double.parseDouble(value));
                        }else{
                          //setNum(workbook, cell, value);
                          cell.setCellValue(Integer.parseInt(value));
                        }
                      }else{
                    	 cell.setCellValue(value);                    	                     
                      }                     
                    }else{
                      cell.setCellValue(value==null?"":value);
                    }                   
                }
            }
        }
    }
    
  public static void setNum(HSSFWorkbook workbook, HSSFCell cell, String str){
    setFormat(workbook, cell, str, "0");
  }
  
  public static void setTwoPoint(HSSFWorkbook workbook, HSSFCell cell, String str) {
      setFormat(workbook, cell, str, "0.00");  
  }

  public static void setFormat(HSSFWorkbook workbook, HSSFCell cell, String str, String formater) {
      HSSFCellStyle s = cell.getCellStyle();
      DataFormat format = workbook.createDataFormat();
      s.setDataFormat(format.getFormat(formater));
      cell.setCellStyle(s);
      cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
      cell.setCellValue(Double.parseDouble(str));
  }
  
    /**
     * 匹配是否为数字
     * @param str
     * @return true 是数字，false 非数字
     */
    public static boolean isNumeric(String str){ 
      Pattern pattern = Pattern.compile("-?[0-9]+.?[0-9]+"); //匹配小数
      Pattern pattern2 = Pattern.compile("[0-9]*"); //匹配整数
      Matcher isNum = pattern.matcher(str);
      Matcher isNum2 = pattern2.matcher(str);
      if( !isNum.matches() && !isNum2.matches()){
          return false; 
      } 
      return true; 
   }
   
   /**
    * 广告单元id 替换为应用名称
    * @param capp
    * @return
    */
   @SuppressWarnings("unchecked")
   public static String replaceAppName(String capp){
	   
	   List<Map<String,Object>> apps = Condition.getApps();
	   String  name = "";
		if(apps!=null && apps.size()>0){
			for(int j=0;j<apps.size();j++){
				String appname = apps.get(j).get("appname")+"";
				if(appname!=null && !"".equals(appname)){
					List<String> units = (List<String>) apps.get(j).get("adunitId");
					if(units.contains(capp)){
						name = appname;
						break;
					}
				}			
			}
		}
		return name;
   }
   
   /**
    * 根据appname在集合中进行分组
    * @param target 待分组数据
    * @return
    */
	public static Map<String, List<List<String>>> groupByappname(List<List<String>> target) {

		// 定义一个map集合用于分组
		Map<String, List<List<String>>> mapList = new HashMap<String, List<List<String>>>();
		
		// 遍历集合以appname为键，值保存到mapList中
		for(int i=0;i<target.size();i++){
			List<String> vlist = target.get(i);
			if(vlist.size()>2){
        		String capp = vlist.get(1);
        		// 如果在这个map中包含有相同的键，这创建一个集合将其存起来
    			if (mapList.containsKey(capp)) {
    				List<List<String>> syn = mapList.get(capp);
    				syn.add(vlist);
    				// 如果没有包含相同的键，在创建一个集合保存数据
    			} else {
    				List<List<String>> syns = new ArrayList<List<String>>();
    				syns.add(vlist);
    				mapList.put(capp, syns);
    			}
        	}
		}
		return mapList;
	}
	
	
	/**
	 * 对分组的数据，重新返回集合中
	 * @param groups
	 * @return
	 * 日期	应用	广告单元	AdMob 广告网络请求次数	匹配请求数	匹配率	展示次数	点击次数	展示点击率	每千次展示收入 ( USD)	估算收入 (USD)
	 */
	public static List<List<String>> getAllGroup(Map<String, List<List<String>>> mapList) {
		
		DecimalFormat df = new DecimalFormat("0.0000");//格式化小数，不足的补0
		//返回的处理好的集合对象 
		List<List<String>> newSynList = new ArrayList<List<String>>();
		// 遍历map集合
		for (Map.Entry<String, List<List<String>>> m : mapList.entrySet()) {
			//合计行
			List<String> totals = new ArrayList<>();
			//admob 广告网络请求次数
			int ad_requests_total =0;
			//匹配请求数
			int matched_ad_requests_total = 0;
			//展示次数
			int individual_ad_impressions_total = 0;
			//点击次数
			int clicks_total=0;
			//每千次展示收入
			double individual_ad_impressions_rpm_total=0.00;
			//估算收入
			double earnings_total=0.00;
			// 获取所有的值,进行合计
			List<List<String>> synList = m.getValue();			
			for(int i=0;i<synList.size();i++){
				List<String> vlist = synList.get(i);				
				if(vlist.size()>10){
					for(int j=0;j<vlist.size();j++){
						//AdMob 广告网络请求次数 统计
						if(j==3){
							ad_requests_total+=Integer.parseInt(vlist.get(j));
						}						
						//匹配请求数 统计
						if(j==4){
							matched_ad_requests_total+=Integer.parseInt(vlist.get(j));
						}					
						//展示次数
						if(j==6){
							individual_ad_impressions_total+=Integer.parseInt(vlist.get(j));					
						}
						//点击次数
						if(j==7){
							clicks_total+=Integer.parseInt(vlist.get(j));
						}
						//每千次展示收入 ( USD)
						if(j==9){
							individual_ad_impressions_rpm_total+=Double.parseDouble(vlist.get(j));
						}
						//估算收入 (USD)
						if(j==10){
							earnings_total+=Double.parseDouble(vlist.get(j));
						}
					}					
				}
			}
			//每组的合计
			if(ad_requests_total>0){			
				totals.add("");
				totals.add("");
				totals.add("");
				totals.add(ad_requests_total+"");
				totals.add(matched_ad_requests_total+"");
				float ad_requests_num = (float)matched_ad_requests_total/ad_requests_total;
				totals.add(df.format(ad_requests_num)+"");
				totals.add(individual_ad_impressions_total+"");
				totals.add(clicks_total+"");
				float ad_impressions_num = (float)clicks_total/individual_ad_impressions_total;
				totals.add(df.format(ad_impressions_num)+"");
				totals.add(individual_ad_impressions_rpm_total+"");
				totals.add(earnings_total+"");
				synList.add(totals);
			}
			newSynList.addAll(synList);
		}
		return newSynList;
	}
}
