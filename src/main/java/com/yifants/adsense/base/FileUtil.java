package com.yifants.adsense.base;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;

public final class FileUtil {
	
  public static String tojson(String fullFileName){
    
    File file = new File(fullFileName);
    Scanner scanner = null;
    StringBuilder buffer = new StringBuilder();
    try {
        scanner = new Scanner(file, "utf-8");
        while (scanner.hasNextLine()) {
            buffer.append(scanner.nextLine());
        }

    } catch (FileNotFoundException e) {
        e.printStackTrace();
    } finally {
        if (scanner != null) {
            scanner.close();
        }
    }
    return buffer.toString();      
}
  
	/**
	 * 
	* @Description: 读取json文件
	* @throws
	 */
	public String ReadFile(String Path) {
		BufferedReader reader = null;
		String laststr = "";
		try {
			InputStream in = Object.class .getResourceAsStream(Path);    
			InputStreamReader inputStreamReader = new InputStreamReader(in, "UTF-8");
			reader = new BufferedReader(inputStreamReader);
			String tempString = null;
			while ((tempString = reader.readLine()) != null) {
				laststr += tempString;
			}
			inputStreamReader.close();
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return laststr;
	}
	
	
	
	/**
	 * 
	* @Description: 获得生成文件的存放目录
	* @throws
	 */
	public String getFileDirectory(){
		URL url = FileUtil.class.getResource("");
        String result = url.toString();
        if(result!=null){
        	int index = result.indexOf("lib"); 
            if(index == -1){ 
            	index = result.indexOf("bin"); 
            } 
            if(index>0){
            	result = result.substring(0,index); 
                if(result.startsWith("jar")){ 
                	// 当class文件在jar文件中时，返回"jar:file:/F:/ ..."样的路径 
                	result = result.substring(10); 
                }else if(result.startsWith("file")){ 
                	// 当class文件在class文件中时，返回"file:/F:/ ..."样的路径 
                	result = result.substring(6); 
                } 
            }else{
            	result ="";
            }
        }  
       return result;
	}
	
	/**
	 * 注意linux和windown下的环境路径
	* @Description: 
	* @throws
	 */
	public String getFileCreateDirectory(int ftype){
		String fileurl = getFileDirectory();
		String osName = System.getProperties().getProperty("os.name").toLowerCase();
		if(null!=fileurl && !fileurl.equals("")){						
			if(osName.contains("linux") || osName.contains("unix")){
				fileurl = File.separator +fileurl;
			}
		}
		if(ftype==0){
			fileurl = fileurl + "appannie"+File.separator;
		}else if(ftype ==1){
			fileurl = fileurl + "appannie"+File.separator + System.currentTimeMillis()+".txt";
		}else if(ftype ==2){			
			if(osName.contains("linux") || osName.contains("unix")){
				fileurl = File.separator +"opt"+File.separator + "appblue" + File.separator+ "static" + File.separator+"appannie_rank.html";
			}else{
				fileurl = (fileurl.contains("/WEB-INF/")?fileurl.replace("/WEB-INF/", ""):fileurl) + File.separator+"appannie_rank.html";
			}
		}	
		System.out.println("========fileurl========"+fileurl);
		return fileurl;
	}
	
	/**
	 * 
	 * @param path 目录
	 */
	public static void mkdirs(String path){
		if(path!=null && !path.equals("")){
			File descPath = new File(path);
		    if (!descPath.exists()) {
		        descPath.mkdirs();
		    }
		}	
	}
	
	public static void main(String[] args) {
		/*File root = new File( "E:/javapro/adsense/lib" );
        File[] files = root.listFiles();
        for ( File file : files )
        {
        	System.out.print(file.getAbsolutePath().replace("E:\\javapro\\adsense\\lib\\", "lib/")+" ");       	     	
        }*/
		  
		System.out.println(new java.io.File(System.getProperty("user.home"), ".store/adsense_management_sample"));
		
	}
}
