package com.yifants.adsense.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;

public final class Condition {

  private static List<Map<String,Object>> apps = null;
  private static String account;
  private static String currency;
  private static String startDate;
  private static String endDate;
  private static String ifuse;
  private static String isgroup;
  private static Map<String,List<String>> units; 
  
  public static void load(String configPath) throws Exception {
    
    if(configPath!=null && !configPath.equals("")){
      if(apps==null){
        apps = new ArrayList<Map<String,Object>>();
      }
      String content = FileUtil.tojson(configPath);     
      if(content!=null &&!content.equals("")){
        JSONObject json = JSON.parseObject(content);
        account = json.getString("account").trim();
        currency = json.getString("currency").trim();
        startDate = json.getString("startDate").trim();
        endDate = json.getString("endDate").trim();
        ifuse = json.getString("ifuse").trim();
        isgroup = json.getString("isgroup").trim();
        apps = JSON.parseObject(json.getString("apps"),new TypeReference<List<Map<String,Object>>>(){});
        System.out.println(json.toString());
      }   
    }     
  }


  public static List<Map<String,Object>> getApps() {
    return apps;
  }


  public static String getAccount() {
    return account;
  }


  public static String getCurrency() {
    return currency;
  }


  public static String getStartDate() {
    return startDate;
  }


  public static String getEndDate() {
    return endDate;
  }


  public static String getIfuse() {
    return ifuse;
  }
  
  public static Map<String,List<String>> getUnits() {
	return units;
}


public static String getIsgroup() {
	return isgroup;
}
  
}
